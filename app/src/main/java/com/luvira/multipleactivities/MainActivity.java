package com.luvira.multipleactivities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    //define the global variables
    Button buttonUno;
    Button buttonDos;
    Button buttonTres;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //initialize the views
        buttonUno = (Button) findViewById(R.id.buttonUno);
        buttonDos = (Button) findViewById(R.id.buttonDos);
        buttonTres = (Button) findViewById(R.id.buttonTres);

    }

    //define the onClick method to handle the views
    public void onClick(View view) {
        //get the view id
        int viewID = view.getId();

        switch (viewID) {
            case R.id.buttonUno:
                //open the first image
                Intent firstIntent = new Intent(this, FirstActivity.class);
                startActivity(firstIntent);
                finish();
                break;
            case R.id.buttonDos:
                //open the second image
                Intent secondIntent = new Intent(this, SecondActivity.class);
                startActivity(secondIntent);
                finish();
                break;
            case R.id.buttonTres:
                //open the third image
                Intent thirdIntent = new Intent(this, ThirdActivity.class);
                startActivity(thirdIntent);
                finish();
                break;
        }
    }
}
